// todo
// - replay finished epochs
// - optimize parallel_for

use rand::SeedableRng;
use sph::{
    helpers::Int3,
    hierarchy::{Hierarchy, IODesc, IOType, LayerDesc},
};
use gym_rs::{
    core::Env, envs::classical_control::cartpole::CartPoleEnv, utils::renderer::RenderMode,
};

/// show window (visual output for humans)
const SHOW_VISUALS_PER_N_EPOCH: usize = 100;

fn main() {
    let mut hier = init_hierarchy();

    let mut rng = rand::rngs::StdRng::from_entropy();

    let mut mins = vec![-0.01; 4];
    let mut maxs = vec![0.01; 4];

    for epoch in 0.. {
	    let mut env = CartPoleEnv::new(if epoch % SHOW_VISUALS_PER_N_EPOCH == 0 {
	        RenderMode::Human
	    } else {
	        RenderMode::None
	    });
        let (mut obs, _) = env.reset(None, false, None);

        let mut died_young = false;
        for step in 0..500 {
            let predictions = hier.prediction_cis(1).clone();

            let obs2: Vec<f64> = obs.into();
            let encoded = encode_obs(&obs2, &mut mins, &mut maxs);

            hier.step(&[&encoded, &predictions], true, 0.0, 0.0, &mut rng);

            let predictions = hier.prediction_cis(1);
            let action = predictions[0] as usize;
            // print!("{action},");

            let state = env.step(action);
            obs = state.observation;

            if state.done {
                if step != 0 {
                    let obs2: Vec<f64> = obs.into();
                    let encoded = encode_obs(&obs2, &mut mins, &mut maxs);
                    let predictions = hier.prediction_cis(1).clone();
                    hier.step(&[&encoded, &predictions], true, -100.0, 0.0, &mut rng);

                    println!("Epoch {epoch} {step}");
                }
                died_young = true;
                break;
            }
        }
        if !died_young {
            println!("Epoch {epoch} finished!");
        }
    }
}

// binning resolution
const OBS_COLUMN_SIZE: usize = 32;

fn init_hierarchy() -> Hierarchy {
    // let num_obs = 4;
    let num_actions = 2;

    let mut rng = rand::rngs::StdRng::from_entropy();
    let io_descs = vec![
        IODesc {
            size: Int3::new(2, 2, OBS_COLUMN_SIZE as i32),
            type_: IOType::None,
            ..Default::default()
        },
        IODesc {
            size: Int3::new(1, 1, num_actions),
            type_: IOType::Action,
            importance: 0.0,
            ..Default::default()
        },
    ];
    let ld = LayerDesc {
        hidden_size: Int3::new(5, 5, 32),
        ..Default::default()
    };
    let lds = vec![ld.clone(), ld];
    Hierarchy::init_random(io_descs, lds, &mut rng)
}


// sacrilegous adaptive encoder
fn encode_obs(obs: &[f64], mins: &mut [f64], maxs: &mut [f64]) -> Vec<i32> {
    (0..obs.len())
        .map(|i| {
            mins[i] = mins[i].min(obs[i]);
            maxs[i] = maxs[i].max(obs[i]);

            // i swear there are better things to do than linear scaling
            ((obs[i] - mins[i]) / (maxs[i] - mins[i]) * (OBS_COLUMN_SIZE - 1) as f64 + 0.5) as i32
        })
        .collect()
}

pub fn sigmoid(x: f64) -> f64 {
    1 as f64 / (1 as f64 + std::f64::consts::E.powf(-x))
}
