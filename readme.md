## what

[Sparse Predictive Hierarchies](https://raw.githubusercontent.com/ogmacorp/OgmaNeo2/master/SPH_Presentation.pdf) ported from [C++](https://github.com/ogmacorp/AOgmaNeo) to Rust.

## how

I don't understand either. It just works.

## example

```
# how to run the example
cargo run  --release --example cartpole
```

**SDL2_gfx** is needed to run this example.

You can edit the example file at `examples/cartpole.rs` to change how often an epoch is displayed visually.

At around 2500 epochs, the thing will occasionally succeed at the task (look for `finished!` in terminal output).
