use crate::helpers::*;

pub type VisibleLayerDesc = crate::actor::VisibleLayerDesc;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct VisibleLayer {
    pub protos: Vec<u8>,
    pub weights: Vec<u8>, // for reconstruction
    pub reconstruction: Vec<u8>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Params {
    /// early stopping threshold distance
    pub threshold: f32,
    /// amount less when not maximal (multiplier)
    pub falloff: f32,
    /// scale of exp
    pub scale: f32,
    /// learning rate
    pub lr: f32,
    /// reconstruction rate
    pub rr: f32,
}
impl Default for Params {
    fn default() -> Self {
        // sus[h5]: how is the default value calculated?
        //          `scale` seems to scale with layer size.
        Self {
            threshold: 0.001,
            scale: 2.0,
            falloff: 0.99,
            lr: 0.1,
            rr: 0.1,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct ImageEncoder {
    /// size of hidden/output layer
    pub hidden_size: Int3,
    /// hidden states
    pub hidden_cis: is,
    pub hidden_resources: fs,

    pub visible_layers: Vec<VisibleLayer>,
    pub visible_layer_descs: Vec<VisibleLayerDesc>,
}
impl ImageEncoder {
    pub fn init_random(
        hidden_size: Int3,
        visible_layer_descs: Vec<VisibleLayerDesc>,
        rng: &mut Rng,
    ) -> Self {
        let mut visible_layers = vec![VisibleLayer::default(); visible_layer_descs.len()];

        // pre-compute dimensions
        let num_hidden_columns = hidden_size.x * hidden_size.y;
        let num_hidden_cells = num_hidden_columns * hidden_size.z;

        for (vld, vl) in visible_layer_descs.iter().zip(&mut visible_layers) {
            let num_visible_columns = vld.size.x * vld.size.y;
            let num_visible_cells = num_visible_columns * vld.size.z;

            let diam = vld.radius * 2 + 1;
            let area = diam * diam;

            let vl_layer_size = (num_hidden_cells * area * vld.size.z) as usize;
            vl.protos = vec![0; vl_layer_size];
            vl.weights = vec![0; vl_layer_size];

            for x in &mut vl.protos {
                *x = rng.gen();
            }
            for x in &mut vl.weights {
                *x = 127;
            }

            vl.reconstruction = vec![0; num_visible_cells as usize];
        }

        let hidden_cis = vec![0; num_hidden_columns as usize];

        let hidden_resources = vec![1.; num_hidden_cells as usize];

        Self {
            hidden_size,
            visible_layer_descs,
            visible_layers,
            hidden_cis,
            hidden_resources,
        }
    }

    // note: not in the original. i hope this is right
    pub fn clear_state(&mut self) {
        self.hidden_cis.fill(0);
        self.hidden_resources.fill(0.);
    }

    /// activate the sparse coder (perform sparse coding)
    /// 
    /// # Params
    /// 
    /// inputs:  inputs for each visible_layer
    pub fn step(
        &mut self,
        inputs: &[&Vec<u8>],
        learn_enabled: bool,
        rng: &mut Rng,
        params: Params,
    ) {
        assert_eq!(self.visible_layer_descs.len(), inputs.len()); // note: added for safety

        let hidden_size = self.hidden_size;

        let num_hidden_columns = hidden_size.x * hidden_size.y;

        let base_seed: u64 = rng.gen();


        // optimize: PARALLEL_FOR
        for i in 0..num_hidden_columns {
            let mut rng = Rng::seed_from_u64(base_seed.wrapping_add(i as u64));
            self.forward(
                Int2::new(i / hidden_size.y, i % hidden_size.y),
                inputs,
                learn_enabled,
                &mut rng,
                params,
            );
        }

        if learn_enabled {
            for vli in 0..self.visible_layer_descs.len() {
                let vld = self.visible_layer_descs[vli];

                let num_visible_columns = vld.size.x * vld.size.y;

                let base_seed: u64 = rng.gen();

                // optimize: PARALLEL_FOR
                for i in 0..num_visible_columns {
                    let mut rng = Rng::seed_from_u64(base_seed.wrapping_add(i as u64));
                    self.learn_reconstruction(
                        Int2::new(i / hidden_size.y, i % hidden_size.y),
                        inputs[vli],
                        vli,
                        &mut rng,
                        params,
                    );
                }
            }
        }
    }

    pub fn reconstruct(&mut self, recon_cis: &is, params: Params) {
        for vli in 0..self.visible_layer_descs.len() {
            let vld = self.visible_layer_descs[vli];

            let num_visible_columns = vld.size.x * vld.size.y;

            // optimize: PARALLEL_FOR
            for i in 0..num_visible_columns {
                self.reconstruct_inner(
                    Int2::new(i / vld.size.y, i % vld.size.y),
                    recon_cis,
                    vli,
                    params,
                );
            }
        }
    }

    pub fn get_reconstruction(&self, vli: usize) -> &Vec<u8> {
        &self.visible_layers[vli].reconstruction
    }

    fn forward(
        &mut self,
        column_pos: Int2,
        inputs: &[&Vec<u8>],
        learn_enabled: bool,
        rng: &mut Rng,
        params: Params,
    ) {
        let Self {
            ref hidden_size,
            ref visible_layer_descs,
            visible_layers,
            hidden_cis,
            ..
        } = self;

        let hidden_column_index = address2(column_pos, Int2::new(hidden_size.x, hidden_size.y));

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        let mut max_index = 0;
        let mut max_activation = 0.0f32;

        let byte_inv = 1.0f32 / 255.0;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            let mut sum = 0.0f32;
            let mut count = 0;

            for ((vld, vl), vl_inputs) in visible_layer_descs
                .iter()
                .zip(visible_layers.iter())
                .zip(inputs)
            {
                let diam = vld.radius * 2 + 1;

                // projection
                let h_to_v = Float2::new(
                    (vld.size.x as f32) / (hidden_size.x as f32),
                    (vld.size.y as f32) / (hidden_size.y as f32),
                );

                let visible_center = project(column_pos, h_to_v);

                // lower corner
                let field_lower_bound =
                    Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

                // bounds of receptive field, clamped to input size
                let iter_lower_bound =
                    Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
                let iter_upper_bound = Int2::new(
                    min(vld.size.x - 1, visible_center.x + vld.radius),
                    min(vld.size.y - 1, visible_center.y + vld.radius),
                );

                count += (iter_upper_bound.x - iter_lower_bound.x + 1)
                    * (iter_upper_bound.y - iter_lower_bound.y + 1)
                    * vld.size.z;

                for ix in iter_lower_bound.x..=iter_upper_bound.x {
                    for iy in iter_lower_bound.y..=iter_upper_bound.y {
                        let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                        let wi_start =
                            vld.size.z * (offset.y + diam * (offset.x + diam * hidden_cell_index));

                        let i_start = vld.size.z * (iy + ix * vld.size.y);

                        // optimize: SIMD
                        for vc in 0..vld.size.z {
                            let wi = vc + wi_start;

                            let input = vl_inputs[(vc + i_start) as usize] as f32 * byte_inv;

                            let w = vl.protos[wi as usize] as f32 * byte_inv;

                            let diff = input - w;

                            sum -= diff * diff;
                        }
                    }
                }
            }

            let sum = sum / max(1, count) as f32;

            if sum > max_activation {
                max_activation = sum;
                max_index = hc;
            }
        }

        hidden_cis[hidden_column_index as usize] = max_index;

        let Self {
            hidden_resources, ..
        } = self;

        if learn_enabled {
            // sus: nan behavior; nan to bool == false
            let scan_radius = ((-max_activation).sqrt() >= params.threshold) as i32;

            for dhc in -scan_radius..=scan_radius {
                let hc = hidden_cis[hidden_column_index as usize] + dhc;

                if hc < 0 || hc >= hidden_size.z {
                    continue;
                }

                let hidden_cell_index = hc + hidden_cells_start;

                let rate = hidden_resources[hidden_cell_index as usize]
                    * (if dhc == 0 { 1.0 } else { params.falloff });

                for ((vld, vl), vl_inputs) in visible_layer_descs
                    .iter()
                    .zip(visible_layers.iter_mut())
                    .zip(inputs)
                {
                    let diam = vld.radius * 2 + 1;

                    // projection
                    let h_to_v = Float2::new(
                        (vld.size.x as f32) / (hidden_size.x as f32),
                        (vld.size.y as f32) / (hidden_size.y as f32),
                    );

                    let visible_center = project(column_pos, h_to_v);

                    // lower corner
                    let field_lower_bound =
                        Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

                    // bounds of receptive field, clamped to input size
                    let iter_lower_bound =
                        Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
                    let iter_upper_bound = Int2::new(
                        min(vld.size.x - 1, visible_center.x + vld.radius),
                        min(vld.size.y - 1, visible_center.y + vld.radius),
                    );

                    for ix in iter_lower_bound.x..=iter_upper_bound.x {
                        for iy in iter_lower_bound.y..=iter_upper_bound.y {
                            let offset =
                                Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                            let wi_start = vld.size.z
                                * (offset.y + diam * (offset.x + diam * hidden_cell_index));

                            let i_start = vld.size.z * (iy + ix * vld.size.y);

                            // optimize: SIMD
                            for vc in 0..vld.size.z {
                                let wi = vc + wi_start;

                                let input = vl_inputs[(vc + i_start) as usize] as f32 * byte_inv;

                                let w = vl.protos[wi as usize] as f32 * byte_inv;

                                vl.protos[wi as usize] = rand_roundf(
                                    rng,
                                    vl.protos[wi as usize] as f32 + rate * 255.0 * (input - w),
                                )
                                .clamp(0., 255.)
                                .as_();
                            }
                        }
                    }
                }

                hidden_resources[hidden_cell_index as usize] -= params.lr * rate;
            }
        }
    }

    fn learn_reconstruction(
        &mut self,
        column_pos: Int2,
        inputs: &[u8],
        vli: usize,
        rng: &mut Rng,
        params: Params,
    ) {
        let Self {
            ref hidden_size,
            ref hidden_cis,
            ref visible_layer_descs,
            visible_layers,
            ..
        } = self;

        let vl = &mut visible_layers[vli];
        let vld = visible_layer_descs[vli];

        let diam = vld.radius * 2 + 1;

        let visible_column_index = address2(column_pos, Int2::new(vld.size.x, vld.size.y));

        let visible_cells_start = visible_column_index * vld.size.z;

        // projection
        let v_to_h = Float2::new(
            (hidden_size.x as f32) / (vld.size.x as f32),
            (hidden_size.y as f32) / (vld.size.y as f32),
        );

        let h_to_v = Float2::new(
            (vld.size.x as f32) / (hidden_size.x as f32),
            (vld.size.y as f32) / (hidden_size.y as f32),
        );

        let reverse_radii = Int2::new(
            (v_to_h.x * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
            (v_to_h.y * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
        );

        let hidden_center = project(column_pos, v_to_h);

        // lower corner
        let field_lower_bound = Int2::new(
            hidden_center.x - reverse_radii.x,
            hidden_center.y - reverse_radii.y,
        );

        // bounds of receptive field, clamped to input size
        let iter_lower_bound = Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
        let iter_upper_bound = Int2::new(
            min(hidden_size.x - 1, hidden_center.x + reverse_radii.x),
            min(hidden_size.y - 1, hidden_center.y + reverse_radii.y),
        );

        let byte_inv = 1.0f32 / 255.0;

        for vc in 0..vld.size.z {
            let visible_cell_index = vc + visible_cells_start;

            let mut sum = 0.0f32;
            let mut count = 0;

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let hidden_pos = Int2::new(ix, iy);

                    let hidden_column_index =
                        address2(hidden_pos, Int2::new(hidden_size.x, hidden_size.y));

                    let visible_center = project(hidden_pos, h_to_v);

                    if in_bounds(
                        column_pos,
                        visible_center - Int2::splat(vld.radius),
                        visible_center + Int2::splat(vld.radius + 1),
                    ) {
                        let hidden_cell_index = hidden_cis[hidden_column_index as usize]
                            + hidden_column_index * hidden_size.z;
                        let offset = column_pos - visible_center + Int2::splat(vld.radius);

                        let wi = vc
                            + (hidden_size.z
                                * (offset.y + diam * (offset.x + diam * hidden_cell_index)));

                        sum += vl.weights[wi as usize] as f32;
                        count += 1;
                    }
                }
            }

            let sum = sum / (max(1, count) * 255) as f32;

            let target = inputs[visible_cell_index as usize] as f32 * byte_inv;

            let _x0 = ((sum - 0.5) * 2.0 * params.scale + 0.5).clamp(0., 1.);

            let delta = params.rr * (target - _x0) * 255.0;

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let hidden_pos = Int2::new(ix, iy);

                    let hidden_column_index =
                        address2(hidden_pos, Int2::new(hidden_size.x, hidden_size.y));

                    let visible_center = project(hidden_pos, h_to_v);

                    if in_bounds(
                        column_pos,
                        visible_center - Int2::splat(vld.radius),
                        visible_center + Int2::splat(vld.radius + 1),
                    ) {
                        let hidden_cell_index = hidden_cis[hidden_column_index as usize]
                            + hidden_column_index * hidden_size.z;
                        let offset = column_pos - visible_center + Int2::splat(vld.radius);

                        let wi = vc
                            + (hidden_size.z
                                * (offset.y + diam * (offset.x + diam * hidden_cell_index)));

                        vl.weights[wi as usize] = rand_roundf(
                            rng,
                            vl.weights[wi as usize] as f32 + delta,
                        )
                        .clamp(0., 255.) as u8;
                    }
                }
            }
        }
    }

    fn reconstruct_inner(&mut self, column_pos: Int2, recon_cis: &is, vli: usize, params: Params) {
        let Self {
            ref hidden_size,
            ref visible_layer_descs,
            visible_layers,
            ..
        } = self;

        let vl = &mut visible_layers[vli];
        let vld = visible_layer_descs[vli];

        let diam = vld.radius * 2 + 1;

        let visible_column_index = address2(column_pos, Int2::new(vld.size.x, vld.size.y));

        let visible_cells_start = visible_column_index * vld.size.z;

        // projection
        let v_to_h = Float2::new(
            (hidden_size.x as f32) / (vld.size.x as f32),
            (hidden_size.y as f32) / (vld.size.y as f32),
        );

        let h_to_v = Float2::new(
            (vld.size.x as f32) / (hidden_size.x as f32),
            (vld.size.y as f32) / (hidden_size.y as f32),
        );

        let reverse_radii = Int2::new(
            (v_to_h.x * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
            (v_to_h.y * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
        );

        let hidden_center = project(column_pos, v_to_h);

        // lower corner
        let field_lower_bound = Int2::new(
            hidden_center.x - reverse_radii.x,
            hidden_center.y - reverse_radii.y,
        );

        // bounds of receptive field, clamped to input size
        let iter_lower_bound = Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
        let iter_upper_bound = Int2::new(
            min(hidden_size.x - 1, hidden_center.x + reverse_radii.x),
            min(hidden_size.y - 1, hidden_center.y + reverse_radii.y),
        );

        for vc in 0..vld.size.z {
            let visible_cell_index = vc + visible_cells_start;

            let mut sum = 0.0f32;
            let mut count = 0;

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let hidden_pos = Int2::new(ix, iy);

                    let hidden_column_index =
                        address2(hidden_pos, Int2::new(hidden_size.x, hidden_size.y));

                    let visible_center = project(hidden_pos, h_to_v);

                    if in_bounds(
                        column_pos,
                        visible_center - Int2::splat(vld.radius),
                        visible_center + Int2::splat(vld.radius + 1),
                    ) {
                        let hidden_cell_index = recon_cis[hidden_column_index as usize]
                            + hidden_column_index * hidden_size.z;
                        let offset = column_pos - visible_center + Int2::splat(vld.radius);

                        let wi = vc
                            + (hidden_size.z
                                * (offset.y + diam * (offset.x + diam * hidden_cell_index)));

                        sum += vl.weights[wi as usize] as f32;
                        count += 1;
                    }
                }
            }

            let sum = sum / (max(1, count) * 255) as f32;

            vl.reconstruction[visible_cell_index as usize] =
                (((sum - 0.5) * 2.0 * params.scale + 0.5).clamp(0., 1.) * 255.0).round() as u8;
        }
    }
}
