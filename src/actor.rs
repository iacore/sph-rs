use crate::helpers::*;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct VisibleLayerDesc {
    pub size: Int3,
    /// radius of 2 creates a 5x5 convolutional filter with sparse columns
    /// sus: should be `usize`
    pub radius: i32,
}
impl Default for VisibleLayerDesc {
    fn default() -> Self {
        Self {
            size: DEFAULT_LAYER_SIZE,
            radius: 2,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct VisibleLayer {
    pub value_weights: fs,
    pub action_weights: fs,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct HistorySample {
    pub input_cis: Vec<is>,
    pub hidden_target_cis_prev: is,
    pub reward: f32,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Params {
    pub vlr: f32,
    pub alr: f32,
    pub discount: f32,
    pub min_steps: usize,
    pub history_iters: usize,
}
impl Default for Params {
    fn default() -> Self {
        Self {
            vlr: 0.01,
            alr: 0.01,
            discount: 0.99,
            min_steps: 8,
            history_iters: 8,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Actor {
    pub hidden_size: Int3,
    pub hidden_acts: fs,
    pub hidden_cis: is,
    pub hidden_values: fs,
    pub history_samples: CircularBuffer<HistorySample>,
    pub visible_layers: Vec<VisibleLayer>,
    pub visible_layer_descs: Vec<VisibleLayerDesc>,
}

/// randomize float weights
fn initialize_floats_to_random(rng: &mut Rng, xs: &mut fs) {
    for x in xs {
        // optimize: fix mantissa bits and set magnitude directly
        *x = (rng.gen::<f32>() * 2. - 1.) * init_weight_noisef;
    }
}

impl Actor {
    pub fn init_random(
        hidden_size: Int3,
        history_capacity: usize,
        visible_layer_descs: Vec<VisibleLayerDesc>,
        rng: &mut Rng,
    ) -> Self {
        let mut visible_layers = vec![VisibleLayer::default(); visible_layer_descs.len()];

        let num_hidden_columns = hidden_size.x * hidden_size.y;
        let num_hidden_cells = num_hidden_columns * hidden_size.z;

        for (vld, vl) in visible_layer_descs.iter().zip(&mut visible_layers) {
            // create weight matrix for this visible layer and initialize randomly

            let diam = vld.radius * 2 + 1;
            let area = diam * diam;

            vl.value_weights = new_floats(num_hidden_columns * area * vld.size.z);

            initialize_floats_to_random(rng, &mut vl.value_weights);

            vl.action_weights = new_floats(num_hidden_cells * area * vld.size.z);

            initialize_floats_to_random(rng, &mut vl.action_weights);
        }

        let hidden_cis = new_ints(num_hidden_columns);
        let hidden_values = new_floats(num_hidden_columns);
        let hidden_acts = new_floats(num_hidden_cells);

        let mut history_samples = CircularBuffer::<HistorySample>::with_capacity(history_capacity);

        for item in &mut history_samples {
            item.input_cis = vec![is::default(); visible_layers.len()];

            for (vld, layer) in visible_layer_descs.iter().zip(&mut item.input_cis) {
                let num_visible_columns = vld.size.x * vld.size.y;

                *layer = new_ints(num_visible_columns);
            }

            item.hidden_target_cis_prev = new_ints(num_hidden_columns);
        }

        Self {
            visible_layers,
            visible_layer_descs,
            hidden_size,
            hidden_cis,
            hidden_values,
            hidden_acts,
            history_samples,
        }
    }

    pub fn clear_state(&mut self) {
        self.hidden_cis.fill(0);
        self.hidden_values.fill(0.);
        self.history_samples.clear();
    }

    fn history_size(&self) -> usize {
        self.history_samples.len()
    }

    pub fn history_capacity(&self) -> usize {
        self.history_samples.capacity()
    }

    pub fn step(
        &mut self,
        input_cis: &Vec<&is>,
        hidden_target_cis_prev: &is,
        reward: f32,
        learn_enabled: bool,
        mimic: f32,
        params: Params,
        rng: &mut Rng,
    ) {
        // forward kernel

        let base_seed: u64 = rng.gen();

        let hidden_size = self.hidden_size;
        let num_hidden_columns = hidden_size.x * hidden_size.y;

        // optimize: PARALLEL_FOR
        for i in 0..num_hidden_columns {
            let mut rng = Rng::seed_from_u64(base_seed.wrapping_add(i as u64));
            self.forward(
                Int2::new(i / hidden_size.y, i % hidden_size.y),
                input_cis,
                &mut rng,
            );
        }

        // add new sample
        let s = HistorySample {
            input_cis: input_cis.iter().map(|x| (*x).clone()).collect(),
            hidden_target_cis_prev: hidden_target_cis_prev.clone(),
            reward,
        };
        // sus[h1]: history is added in reverse ...
        push_front_overwriting(&mut self.history_samples, s);

        // learn (if have sufficient samples)
        if learn_enabled && self.history_size() > params.min_steps {
            for it in 0..params.history_iters {
                _ = it;

                // t >= params.min_steps
                let t = rng.gen_range(params.min_steps..self.history_size());

                // compute (partial) values, rest is completed in the kernel
                let mut r: f32 = 0.0;
                let mut d: f32 = 1.0;

                // note: this loop seems to touch [0,t] columns
                // note[h1]: ...and iterated in reverse. 這寫法有毛病
                for t2 in (0..t).rev() {
                    r += self.history_samples[t2].reward * d;
                    d *= params.discount;
                }

                // optimize: PARALLEL_FOR
                for i in 0..num_hidden_columns {
                    self.learn(
                        Int2::new(i / hidden_size.y, i % hidden_size.y),
                        t,
                        r,
                        d,
                        mimic,
                        params,
                    );
                }
            }
        }
    }

    fn forward(&mut self, column_pos: Int2, input_cis: &Vec<&is>, rng: &mut Rng) {
        let Self {
            hidden_acts,
            hidden_cis,
            hidden_size,
            hidden_values,
            ref visible_layer_descs,
            ref visible_layers,
            ..
        } = self;

        let hidden_column_index = address2(column_pos, hidden_size.xy());

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        // clear
        hidden_acts[hidden_cells_start.as_()..(hidden_cells_start + hidden_size.z) as usize]
            .fill(0.);

        let mut value = 0.0f32;
        let mut count = 0;

        for ((vld, vl), vl_input_cis) in visible_layer_descs
            .iter()
            .zip(visible_layers)
            .zip(input_cis)
        {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            count += (iter_upper_bound.x - iter_lower_bound.x + 1)
                * (iter_upper_bound.y - iter_lower_bound.y + 1);

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci = vl_input_cis[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_value = offset.y
                        + diam * (offset.x + diam * (in_ci + vld.size.z * hidden_column_index));
                    let wi_start = hidden_size.z * wi_value;

                    for hc in 0..hidden_size.z {
                        let hidden_cell_index = hc + hidden_cells_start;
                        let wi = hc + wi_start;
                        // optimize: SIMD
                        // sus[h2]: (see other comment)
                        hidden_acts[hidden_cell_index as usize] += vl.action_weights[wi as usize];
                    }

                    value += vl.value_weights[wi_value as usize];
                }
            }
        }

        let count = count as f32;
        let value = value / count;

        hidden_values[hidden_column_index as usize] = value;

        let mut max_activation = limit_min;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] /= count;

            max_activation = f32::max(max_activation, hidden_acts[hidden_cell_index as usize]);
        }

        let mut total = 0f32;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            let prev: f32 = hidden_acts[hidden_cell_index as usize];
            hidden_acts[hidden_cell_index as usize] = (prev - max_activation).exp();

            total += hidden_acts[hidden_cell_index as usize];
        }

        let cusp = rng.gen::<f32>() * total;

        let mut select_index = 0;
        let mut sum_so_far = 0.0f32;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            sum_so_far += hidden_acts[hidden_cell_index as usize];

            if sum_so_far >= cusp {
                select_index = hc;

                break;
            }
        }

        hidden_cis[hidden_column_index as usize] = select_index.as_();
    }

    /// # Parameters
    /// t       index into self.history_samples
    /// mimic   how much the actor learns from example. between [0,1]. when 0, relies on sd_error to learn. when 1, completely learns from example.
    fn learn(&mut self, column_pos: Int2, t: usize, r: f32, d: f32, mimic: f32, params: Params) {
        let Self {
            hidden_acts,
            ref hidden_size,
            ref hidden_values,
            ref history_samples,
            ref visible_layer_descs,
            visible_layers,
            ..
        } = self;

        let hidden_column_index = address2(column_pos, hidden_size.xy());

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        let history_current = &history_samples[t];

        let target_ci = history_samples[t - 1].hidden_target_cis_prev[hidden_column_index as usize];

        // --- value prev ---

        let new_value = r + d * hidden_values[hidden_column_index as usize];

        let mut value = 0.0f32;
        let mut count = 0;

        for ((vld, vl), vl_input_cis) in visible_layer_descs
            .iter()
            .zip(visible_layers.iter())
            .zip(&history_current.input_cis)
        {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            count += (iter_upper_bound.x - iter_lower_bound.x + 1)
                * (iter_upper_bound.y - iter_lower_bound.y + 1);

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci = vl_input_cis[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_value = offset.y
                        + diam * (offset.x + diam * (in_ci + vld.size.z * hidden_column_index));
                    value += vl.value_weights[wi_value as usize];
                }
            }
        }

        let count = count as f32;
        let value = value / count;

        let td_error_value = new_value - value;

        // clear
        hidden_acts[hidden_cells_start as usize..(hidden_cells_start + hidden_size.z) as usize]
            .fill(0.);

        let delta_value = params.vlr * td_error_value;

        for ((vld, vl), vl_input_cis) in visible_layer_descs
            .iter()
            .zip(visible_layers.iter_mut())
            .zip(&history_current.input_cis)
        {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci = vl_input_cis[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_value = offset.y
                        + diam * (offset.x + diam * (in_ci + vld.size.z * hidden_column_index));

                    let wi_start = hidden_size.z * wi_value;

                    for hc in 0..hidden_size.z {
                        let hidden_cell_index = hc + hidden_cells_start;
                        let wi = hc + wi_start;
                        // optimize: SIMD
                        hidden_acts[hidden_cell_index as usize] += vl.action_weights[wi as usize];
                    }

                    vl.value_weights[wi_value as usize] += delta_value;
                }
            }
        }

        // --- action ---

        let mut max_activation = limit_min;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] /= count;

            max_activation = f32::max(max_activation, hidden_acts[hidden_cell_index as usize]);
        }

        let mut total = 0.0f32;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] =
                (hidden_acts[hidden_cell_index as usize] - max_activation).exp();

            total += hidden_acts[hidden_cell_index as usize];
        }

        let total_inv = 1.0 / f32::max(limit_small, total);

        // sus: why tanh is used here? better squashing function exist. better = more spread out the values.
        let rate = params.alr * (mimic + (1.0 - mimic) * td_error_value.tanh());

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] *= total_inv;

            // re-use as deltas
            hidden_acts[hidden_cell_index as usize] =
                rate * ((hc == target_ci) as u8 as f32 - hidden_acts[hidden_cell_index as usize]);
        }

        for ((vld, vl), vl_input_cis) in visible_layer_descs
            .iter()
            .zip(visible_layers)
            .zip(&history_current.input_cis)
        {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci = vl_input_cis[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_value = offset.y
                        + diam * (offset.x + diam * (in_ci + vld.size.z * hidden_column_index));
                    let wi_start = hidden_size.z * wi_value;

                    for hc in 0..hidden_size.z {
                        let hidden_cell_index = hc + hidden_cells_start;
                        let wi = hc + wi_start;
                        // optimize: SIMD
                        // sus[h2]: this adding back and forth action I don't understand
                        vl.action_weights[wi as usize] += hidden_acts[hidden_cell_index as usize];
                    }
                }
            }
        }
    }
}
