#![allow(non_camel_case_types, non_upper_case_globals)]

pub use num_traits::AsPrimitive;
pub use rand::prelude::StdRng as Rng;
pub use rand::{Rng as _Rng, SeedableRng};
pub use serde::{Deserialize, Serialize};
pub use std::cmp::{max, min};
pub use std::collections::VecDeque as CircularBuffer;
pub use std::num::Wrapping;

// pub(crate) const pi: f32 = std::f32::consts::PI;
// pub(crate) const pi2: f32 = pi * 2.;
// pub(crate) const pi_over_2: f32 = pi / 2.;
// pub(crate) const log2_e: f32 = std::f32::consts::LOG2_E;
// pub(crate) const log2_e_inv: f32 = 1.0 / log2_e;

// magic numbers
pub(crate) const limit_min: f32 = -1e6;
// pub(crate) const limit_max: f32 = 1e6;
pub(crate) const limit_small: f32 = 1e-6;
// note[h4]: (see other)
pub(crate) const init_weight_noisei: u8 = 9;
pub(crate) const init_weight_noisef: f32 = 0.01;

pub type fs = Vec<f32>;

// sus: should be a list of >=0 numbers but stored as i32 here
// optimize: maybe u16 will do?
pub type is = Vec<i32>;

pub struct UnitMarker;

/// mostly used to represent CSDR column position, and position offset
pub type Int2 = euclid::default::Vector2D<i32>;

/// mostly used to represent network layer size
pub type Int3 = euclid::default::Vector3D<i32>;

pub const DEFAULT_LAYER_SIZE: Int3 = Int3::new(4, 4, 16);

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Float2 {
    pub x: f32,
    pub y: f32,
}
impl Float2 {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}

pub(crate) fn new_ints(len: impl AsPrimitive<usize>) -> is {
    vec![0; len.as_()]
}

pub(crate) fn new_floats(len: impl AsPrimitive<usize>) -> fs {
    vec![0.; len.as_()]
}

pub(crate) fn address2(pos: Int2, dims: Int2) -> i32 {
    pos.y + pos.x * dims.y
}

pub(crate) fn in_bounds(pos: Int2, lower_bound: Int2, upper_bound: Int2) -> bool {
    pos.x >= lower_bound.x
        && pos.x < upper_bound.x
        && pos.y >= lower_bound.y
        && pos.y < upper_bound.y
}

pub(crate) fn project(pos: Int2, to_scalars: Float2) -> Int2 {
    Int2::new(
        ((pos.x as f32 + 0.5) * to_scalars.x).as_(),
        ((pos.y as f32 + 0.5) * to_scalars.y).as_(),
    )
}

pub(crate) fn rand_roundf(rng: &mut Rng, x: f32) -> f32 {
    let i = x.trunc();
    let abs_rem = (x - i).abs();

    let s = x.signum();

    i + (rng.gen::<f32>() < abs_rem) as u8 as f32 * s
}

pub(crate) fn push_front_overwriting<T>(xs: &mut CircularBuffer<T>, s: T) {
    if xs.len() == xs.capacity() {
        _ = xs.pop_back();
    }
    xs.push_front(s);
}
