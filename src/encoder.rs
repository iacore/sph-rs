use crate::helpers::*;

pub type VisibleLayerDesc = crate::actor::VisibleLayerDesc;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VisibleLayer {
    pub weights: Vec<u8>,
    pub recon_sums: is,
    pub recon_deltas: fs,
    pub importance: f32,
}

impl Default for VisibleLayer {
    fn default() -> Self {
        Self {
            weights: Default::default(),
            recon_sums: Default::default(),
            recon_deltas: Default::default(),
            // note: potential footgun
            importance: 1.,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Params {
    /// scale of exp
    pub scale: f32,
    /// learning rate
    pub lr: f32,
    /// gate curve
    pub gcurve: f32,
}
impl Default for Params {
    fn default() -> Self {
        // sus[h5]: how is the default value calculated?
        //          `scale` seems to scale with layer size.
        Self {
            scale: 8.0,
            lr: 0.02,
            gcurve: 16.0,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Encoder {
    /// size of hidden/output layer
    pub hidden_size: Int3,
    pub hidden_cis: is,
    // sus[h6]: hidden_acts acts like Decoder::hidden_sums (sum of something), but it's called hidden_acts. no idea what it means
    pub hidden_acts: fs,

    pub hidden_gates: fs,

    pub visible_layers: Vec<VisibleLayer>,
    pub visible_layer_descs: Vec<VisibleLayerDesc>,

    /// cartesian product of column coordinates and visible layers. for parallelization.
    pub visible_pos_vlis: Vec<Int3>,
}
impl Encoder {
    pub fn init_random(
        hidden_size: Int3,
        visible_layer_descs: Vec<VisibleLayerDesc>,
        rng: &mut Rng,
    ) -> Self {
        let mut visible_layers = vec![VisibleLayer::default(); visible_layer_descs.len()];

        // pre-compute dimensions
        let num_hidden_columns = hidden_size.x * hidden_size.y;
        let num_hidden_cells = num_hidden_columns * hidden_size.z;

        let mut total_num_visible_columns = 0;

        for (vld, vl) in visible_layer_descs.iter().zip(&mut visible_layers) {
            let num_visible_columns = vld.size.x * vld.size.y;
            let num_visible_cells = num_visible_columns * vld.size.z;

            total_num_visible_columns += num_visible_columns;

            let diam = vld.radius * 2 + 1;
            let area = diam * diam;

            vl.weights = vec![0; (num_hidden_cells * area * vld.size.z) as usize];

            for x in &mut vl.weights {
                *x = 255u8 - (rng.gen::<u8>() % init_weight_noisei);
            }

            vl.recon_sums = vec![0; num_visible_cells as usize];

            vl.recon_deltas = vec![0.; num_visible_cells as usize];
        }

        let hidden_cis = vec![0; num_hidden_columns as usize];

        let hidden_acts = vec![0.; num_hidden_cells as usize];

        let hidden_gates = vec![0.; num_hidden_columns as usize];

        // generate helper buffers for parallelization
        let mut visible_pos_vlis = vec![Int3::default(); total_num_visible_columns as usize];

        let mut index = 0;

        for (vli, vld) in visible_layer_descs
            .iter()
            .enumerate()
        {
            let num_visible_columns = vld.size.x * vld.size.y;

            for i in 0..num_visible_columns {
                visible_pos_vlis[index] = Int3::new(i / vld.size.y, i % vld.size.y, vli.as_());
                index += 1;
            }
        }

        Self {
            hidden_size,
            visible_layer_descs,
            visible_layers,
            hidden_cis,
            hidden_acts,
            hidden_gates,
            visible_pos_vlis,
        }
    }

    pub fn clear_state(&mut self) {
        self.hidden_cis.fill(0);
    }

    pub fn step(&mut self, input_cis: &[&is], learn_enabled: bool, rng: &mut Rng, params: Params) {
        let hidden_size = self.hidden_size;

        let num_hidden_columns = hidden_size.x * hidden_size.y;

        // optimize: PARALLEL_FOR
        for i in 0..num_hidden_columns {
            self.forward(
                Int2::new(i / hidden_size.y, i % hidden_size.y),
                input_cis,
            );
        }

        if learn_enabled {
            // optimize: PARALLEL_FOR
            for i in 0..num_hidden_columns {
                self.update_gates(Int2::new(i / hidden_size.y, i % hidden_size.y), params);
            }

            let base_seed: u64 = rng.gen();

            // optimize: PARALLEL_FOR
            for i in 0..self.visible_pos_vlis.len() {
                let pos = self.visible_pos_vlis[i].xy();
                let vli = self.visible_pos_vlis[i].z as usize;

                let mut rng = Rng::seed_from_u64(base_seed.wrapping_add(i as u64));

                self.learn(pos, input_cis[vli], vli, &mut rng, params);
            }
        }
    }

    fn forward(&mut self, column_pos: Int2, input_cis: &[&is]) {
        let Self {
            ref hidden_size,
            ref visible_layers,
            ref visible_layer_descs,
            hidden_acts,
            hidden_cis,
            ..
        } = self;

        let hidden_column_index = address2(column_pos, Int2::new(hidden_size.x, hidden_size.y));

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] = 0.;
        }

        for ((vld, vl), vl_input_cis) in visible_layer_descs
            .iter()
            .zip(visible_layers)
            .zip(input_cis)
        {
            if vl.importance == 0. {
                continue;
            }

            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            let sub_count = (iter_upper_bound.x - iter_lower_bound.x + 1)
                * (iter_upper_bound.y - iter_lower_bound.y + 1);

            let hidden_stride = vld.size.z * diam * diam;

            let influence = vl.importance / (sub_count as f32 * 255.);

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci = vl_input_cis[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_offset = in_ci + vld.size.z * (offset.y + diam * offset.x);

                    // optimize: SIMD
                    for hc in 0..hidden_size.z {
                        let hidden_cell_index = hc + hidden_cells_start;
                        let wi = wi_offset + hidden_cell_index * hidden_stride;
                        
                        hidden_acts[hidden_cell_index as usize] += vl.weights[wi as usize] as f32 * influence;
                    }
                }
            }
        }

        // index of the cell with max activation
        let mut max_index = 0;
        let mut max_activation = 0.0f32;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            let activation = hidden_acts[hidden_cell_index as usize];

            if activation > max_activation {
                max_activation = activation;
                max_index = hc;
            }
        }

        hidden_cis[hidden_column_index as usize] = max_index.as_();
    }

    fn update_gates(&mut self, column_pos: Int2, params: Params) {
        let Self {
            visible_layers,
            ref visible_layer_descs,
            ref hidden_size,
            ref hidden_cis,
            hidden_gates,
            ..
        } = self;

        let hidden_column_index = address2(column_pos, Int2::new(hidden_size.x, hidden_size.y));

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        let hidden_cell_index_max = hidden_cis[hidden_column_index as usize] + hidden_cells_start;

        let byte_inv = 1.0f32 / 255.0;

        let mut sum = 0.0f32;
        let mut count = 0;

        for (vld, vl) in visible_layer_descs.iter().zip(visible_layers) {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            count += (iter_upper_bound.x - iter_lower_bound.x + 1)
                * (iter_upper_bound.y - iter_lower_bound.y + 1)
                * vld.size.z;

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let ixy = Int2::new(ix, iy);

                    let offset = ixy - field_lower_bound;

                    let wi_start = vld.size.z
                        * (offset.y + diam * (offset.x + diam * hidden_cell_index_max));

                    // optimize: SIMD
                    for vc in 0..vld.size.z {
                        let wi = vc + wi_start;
                        let w = (255.0 - vl.weights[wi as usize] as f32) * byte_inv;
                        sum += w.abs();
                    }
                }
            }
        }

        let sum = sum / count as f32;

        hidden_gates[hidden_column_index as usize] = (-sum * params.gcurve).exp();
    }

    fn learn(
        &mut self,
        column_pos: Int2,
        input_cis: &is,
        vli: usize,
        rng: &mut Rng,
        params: Params,
    ) {
        let Self {
            visible_layers,
            ref visible_layer_descs,
            ref hidden_size,
            ref hidden_cis,
            ref hidden_gates,
            ..
        } = self;

        let vl = &mut visible_layers[vli];
        let vld = visible_layer_descs[vli];

        if vl.importance == 0. {
            return;
        }

        let diam = vld.radius * 2 + 1;

        let visible_column_index = address2(column_pos, Int2::new(vld.size.x, vld.size.y));

        let visible_cells_start = visible_column_index * vld.size.z;

        // projection
        let v_to_h = Float2::new(
            (hidden_size.x as f32) / (vld.size.x as f32),
            (hidden_size.y as f32) / (vld.size.y as f32),
        );

        let h_to_v = Float2::new(
            (vld.size.x as f32) / (hidden_size.x as f32),
            (vld.size.y as f32) / (hidden_size.y as f32),
        );

        let reverse_radii = Int2::new(
            (v_to_h.x * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
            (v_to_h.y * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
        );

        let hidden_center = project(column_pos, v_to_h);

        // lower corner
        let field_lower_bound = Int2::new(
            hidden_center.x - reverse_radii.x,
            hidden_center.y - reverse_radii.y,
        );

        // bounds of receptive field, clamped to input size
        let iter_lower_bound = Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
        let iter_upper_bound = Int2::new(
            min(hidden_size.x - 1, hidden_center.x + reverse_radii.x),
            min(hidden_size.y - 1, hidden_center.y + reverse_radii.y),
        );
        let target_ci = input_cis[visible_column_index as usize];

        // clear
        for vc in 0..vld.size.z {
            let visible_cell_index = vc + visible_cells_start;

            vl.recon_sums[visible_cell_index as usize] = 0;
        }

        let mut count = 0;

        for ix in iter_lower_bound.x..=iter_upper_bound.x {
            for iy in iter_lower_bound.y..=iter_upper_bound.y {
                let hidden_pos = Int2::new(ix, iy);

                let hidden_column_index =
                    address2(hidden_pos, Int2::new(hidden_size.x, hidden_size.y));

                let visible_center = project(hidden_pos, h_to_v);

                if in_bounds(
                    column_pos,
                    visible_center - Int2::splat(vld.radius),
                    visible_center + Int2::splat(vld.radius + 1),
                ) {
                    let hidden_cell_index_max: i32 = hidden_cis[hidden_column_index as usize]
                        + hidden_column_index * hidden_size.z;

                    let offset = column_pos - visible_center + Int2::splat(vld.radius);

                    let wi_start =
                        vld.size.z * (offset.y + diam * (offset.x + diam * hidden_cell_index_max));

                    // optimize: SIMD
                    for vc in 0..vld.size.z {
                        let visible_cell_index = vc + visible_cells_start;

                        let wi = vc + wi_start;

                        vl.recon_sums[visible_cell_index as usize] +=
                            vl.weights[wi as usize] as i32;
                    }

                    count += 1;
                }
            }
        }

        let mut max_index = 0;
        let mut max_activation = 0;

        for vc in 0..vld.size.z {
            let visible_cell_index = vc + visible_cells_start;

            let recon_sum = vl.recon_sums[visible_cell_index as usize];

            if recon_sum > max_activation {
                max_activation = recon_sum;
                max_index = vc;
            }

            vl.recon_deltas[visible_cell_index as usize] = params.lr
                * 255.0
                * ((vc == target_ci) as u8 as f32
                    - ((recon_sum as f32 / max(1, count * 255) as f32 - 1.0) * params.scale).exp());
        }

        if max_index == target_ci {
            return;
        }

        for ix in iter_lower_bound.x..=iter_upper_bound.x {
            for iy in iter_lower_bound.y..=iter_upper_bound.y {
                let hidden_pos = Int2::new(ix, iy);

                let hidden_column_index =
                    address2(hidden_pos, Int2::new(hidden_size.x, hidden_size.y));

                let visible_center = project(hidden_pos, h_to_v);

                if in_bounds(
                    column_pos,
                    visible_center - Int2::splat(vld.radius),
                    visible_center + Int2::splat(vld.radius + 1),
                ) {
                    let hidden_cell_index_max = hidden_cis[hidden_column_index as usize]
                        + hidden_column_index * hidden_size.z;

                    let offset = column_pos - visible_center + Int2::splat(vld.radius);

                    let wi_start =
                        vld.size.z * (offset.y + diam * (offset.x + diam * hidden_cell_index_max));

                    let gate = hidden_gates[hidden_column_index as usize];

                    // optimize: SIMD
                    for vc in 0..vld.size.z {
                        let visible_cell_index = vc + visible_cells_start;

                        let wi = vc + wi_start;

                        vl.weights[wi as usize] = rand_roundf(
                            rng,
                            vl.weights[wi as usize] as f32
                                + vl.recon_deltas[visible_cell_index as usize] * gate,
                        )
                        .clamp(0., 255.) as u8;
                    }
                }
            }
        }
    }
}
