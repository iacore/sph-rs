use crate::actor::Actor;
use crate::decoder::Decoder;
use crate::encoder::Encoder;
use crate::helpers::*;
use crate::{actor, decoder, encoder};

/// This specifies the type of the IO layer (IO port).
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum IOType {
    #[default]
    /// no prediction or action is generated, it's input-only.
    None,
    /// a regular t+1 prediction is generated, irrespective of the reward.
    Prediction,
    /// an action-prediction, like prediction but affected by reward.
    Action,
}
use IOType::*;

/// input-output descriptor
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct IODesc {
    pub size: Int3,

    pub type_: IOType,

    /// encoder radius
    pub up_radius: i32,
    /// decoder radius, also shared with actor if there is one
    pub down_radius: i32,

    /// actor history max window size
    pub history_capacity: usize,

    /// during training, should the model try to learn from history of this layer's input/output?
    pub importance: f32,
}
impl Default for IODesc {
    fn default() -> Self {
        Self {
            size: DEFAULT_LAYER_SIZE,
            type_: Prediction,
            up_radius: 2,
            down_radius: 2,
            history_capacity: 128,
            importance: 1.,
        }
    }
}

/// layer descriptor
/// describes a layer for construction. for the first layer, the IO_Desc overrides the parameters that are the same name
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct LayerDesc {
    /// size of hidden layer
    pub hidden_size: Int3,

    /// encoder radius
    pub up_radius: i32,
    /// decoder radius, also shared with actor if there is one
    pub down_radius: i32,

    /// number of ticks a layer takes to update (relative to previous layer)
    pub ticks_per_update: usize,
    /// temporal distance into the past addressed by the layer. should be greater than or equal to ticks_per_update
    pub temporal_horizon: usize,
}
impl Default for LayerDesc {
    fn default() -> Self {
        Self {
            hidden_size: DEFAULT_LAYER_SIZE,
            up_radius: 2,
            down_radius: 2,
            ticks_per_update: 2,
            temporal_horizon: 2,
        }
    }
}

#[derive(Debug, Default, Clone, Copy, Serialize, Deserialize)]
pub struct LayerParams {
    pub decoder: decoder::Params,
    pub encoder: encoder::Params,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct IoParams {
    pub decoder: decoder::Params,
    pub actor: actor::Params,
    // additional
    pub importance: f32,
}
impl Default for IoParams {
    fn default() -> Self {
        Self {
            decoder: Default::default(),
            actor: Default::default(),
            importance: 1.,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Params {
    pub layers: Vec<LayerParams>,
    pub ios: Vec<IoParams>,
}

// sus: derive(Default) creates an empty hierarchy
#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Hierarchy {
    // layers
    pub encoders: Vec<Encoder>,
    pub decoders: Vec<Vec<Decoder>>,
    pub actors: Vec<Actor>,

    // for mapping first layer Decoders
    // i_indices[d_index] = i;
    // d_indices[i] = d_index;
    // d_index: decoder index, count up from zero
    // i: io_desc[i]
    pub i_indices: Vec<usize>,
    pub d_indices: Vec<usize>,

    /// histories
    // sus: extreme footgun: do not use `histories[i].len`! C++ .size() -> Rust .capacity()
    pub histories: Vec<Vec<CircularBuffer<is>>>,

    /// per-layer values
    pub updates: Vec<bool>,

    pub ticks: Vec<usize>,
    pub ticks_per_update: Vec<usize>,

    /// input dimensions
    pub io_sizes: Vec<Int3>,
    pub io_types: Vec<IOType>,

    pub params: Params,
}
impl Hierarchy {
    /// create a randomly initialized hierarchy
    pub fn init_random(io_descs: Vec<IODesc>, layer_descs: Vec<LayerDesc>, rng: &mut Rng) -> Self {
        let mut encoders: Vec<Encoder> = vec![Default::default(); layer_descs.len()];
        let mut decoders: Vec<Vec<Decoder>> = vec![Default::default(); layer_descs.len()];
        let ticks = vec![0; layer_descs.len()];
        let mut histories: Vec<Vec<CircularBuffer<is>>> =
            vec![Default::default(); layer_descs.len()];
        let mut ticks_per_update = vec![Default::default(); layer_descs.len()];
        // default update state is no update
        let updates = vec![false; layer_descs.len()];
        // cache input sizes
        let mut io_sizes = vec![Default::default(); io_descs.len()];
        let mut io_types = vec![Default::default(); io_descs.len()];
        let mut ios: Vec<IoParams> = vec![Default::default(); io_descs.len()];
        for (io, desc) in ios.iter_mut().zip(&io_descs) {
            io.importance = desc.importance;
        }
        let params = Params {
            layers: vec![Default::default(); layer_descs.len()],
            ios,
        };

        // determine ticks per update, first layer is always 1
        for l in 0..layer_descs.len() {
            let mut x = layer_descs[l].ticks_per_update;
            if l == 0 {
                x = 1;
            }
            ticks_per_update[l] = x;
        }

        let mut num_predictions = 0;
        let mut num_actions = 0;

        for i in 0..io_sizes.len() {
            io_sizes[i] = io_descs[i].size;
            io_types[i] = io_descs[i].type_;

            if io_descs[i].type_ == Prediction {
                num_predictions += 1;
            } else if io_descs[i].type_ == Action {
                num_actions += 1;
            }
        }

        let mut actors = vec![Default::default(); num_actions];

        let mut i_indices = vec![0; io_sizes.len() * 2];
        let mut d_indices = vec![Self::D_INDEX_INVALID; io_sizes.len()];

        // iterate through layers
        for l in 0..layer_descs.len() {
            // create sparse coder visible layer descriptors
            let mut e_visible_layer_descs: Vec<encoder::VisibleLayerDesc>;

            // if first layer
            if l == 0 {
                e_visible_layer_descs =
                    vec![Default::default(); io_sizes.len() * layer_descs[l].temporal_horizon];

                for i in 0..io_sizes.len() {
                    for t in 0..layer_descs[l].temporal_horizon {
                        let index = t + layer_descs[l].temporal_horizon * i;

                        e_visible_layer_descs[index].size = io_sizes[i];
                        e_visible_layer_descs[index].radius = io_descs[i].up_radius;
                    }
                }

                // initialize history buffers
                let h_l = &mut histories[l];
                *h_l = vec![Default::default(); io_sizes.len()];

                for (i, h_li) in h_l.iter_mut().enumerate() {
                    let in_size = io_sizes[i].x * io_sizes[i].y;

                    *h_li = CircularBuffer::with_capacity(layer_descs[l].temporal_horizon);

                    // note: done here to pass empty history to encoders when not enough history is gathered yet
                    while h_li.len() != h_li.capacity() {
                        h_li.push_back(vec![0; in_size as usize]);
                    }
                }

                decoders[l] = vec![Default::default(); num_predictions];

                // create decoders and actors
                let mut d_index = 0;

                for i in 0..io_sizes.len() {
                    if io_descs[i].type_ == Prediction {
                        // decoder visible layer descriptors
                        let mut d_visible_layer_descs: Vec<decoder::VisibleLayerDesc> =
                            vec![Default::default(); 1 + (l < encoders.len() - 1) as usize];

                        d_visible_layer_descs[0].size = layer_descs[l].hidden_size;
                        d_visible_layer_descs[0].radius = io_descs[i].down_radius;

                        if l < encoders.len() - 1 {
                            d_visible_layer_descs[1] = d_visible_layer_descs[0];
                        }

                        decoders[l][d_index] =
                            Decoder::init_random(io_sizes[i], d_visible_layer_descs, rng);

                        i_indices[d_index] = i;
                        d_indices[i] = d_index;
                        d_index += 1;
                    }
                }

                let mut d_index = 0;

                for i in 0..io_sizes.len() {
                    if io_descs[i].type_ == Action {
                        // decoder visible layer descriptors
                        let mut a_visible_layer_descs: Vec<actor::VisibleLayerDesc> =
                            vec![Default::default(); 1 + (l < encoders.len() - 1) as usize];

                        a_visible_layer_descs[0].size = layer_descs[l].hidden_size;
                        a_visible_layer_descs[0].radius = io_descs[i].down_radius;

                        if l < encoders.len() - 1 {
                            a_visible_layer_descs[1] = a_visible_layer_descs[0];
                        }

                        actors[d_index] = Actor::init_random(
                            io_sizes[i],
                            io_descs[i].history_capacity,
                            a_visible_layer_descs,
                            rng,
                        );

                        i_indices[io_sizes.len() + d_index] = i;
                        d_indices[i] = d_index;
                        d_index += 1;
                    }
                }
            } else {
                // l >= 1
                e_visible_layer_descs = vec![Default::default(); layer_descs[l].temporal_horizon];

                for x in e_visible_layer_descs.iter_mut() {
                    x.size = layer_descs[l - 1].hidden_size;
                    x.radius = layer_descs[l].up_radius;
                }

                // let in_size = layer_descs[l - 1].hidden_size.x * layer_descs[l - 1].hidden_size.y;

                let h0 = CircularBuffer::with_capacity(layer_descs[l].temporal_horizon);

                // sus: here in original code: "set each element of h0 to vec![0; in_size]"
                //      the code still seems to work without it

                histories[l] = vec![h0];

                decoders[l] = vec![Default::default(); layer_descs[l].ticks_per_update];

                // decoder visible layer descriptors
                let mut d_visible_layer_descs: Vec<decoder::VisibleLayerDesc> =
                    vec![Default::default(); 1 + (l < encoders.len() - 1) as usize];

                d_visible_layer_descs[0].size = layer_descs[l].hidden_size;
                d_visible_layer_descs[0].radius = layer_descs[l].down_radius;

                if l < encoders.len() - 1 {
                    d_visible_layer_descs[1] = d_visible_layer_descs[0];
                }

                // create decoders
                for decoder in &mut decoders[l] {
                    *decoder = Decoder::init_random(
                        layer_descs[l - 1].hidden_size,
                        d_visible_layer_descs.clone(),
                        rng,
                    );
                }
            }

            // create the sparse coding layer
            encoders[l] =
                Encoder::init_random(layer_descs[l].hidden_size, e_visible_layer_descs, rng);
        }

        Self {
            encoders,
            decoders,
            ticks,
            histories,
            ticks_per_update,
            updates,
            io_sizes,
            io_types,
            params,
            actors,
            i_indices,
            d_indices,
        }
    }

    /// simulation step/tick
    pub fn step(
        &mut self,
        input_cis: &[&is],   // inputs to remember
        learn_enabled: bool, // = true // whether learning is enabled
        reward: f32,         // = 0. // reward
        mimic: f32,          // = 0. // mimicry mode (can set to 1)
        rng: &mut Rng,
    ) {
        let Self {
            ref params,
            ref io_sizes,
            encoders,
            ..
        } = self;

        assert!(params.layers.len() == encoders.len());
        assert!(params.ios.len() == io_sizes.len());

        // set importances from params
        for i in 0..io_sizes.len() {
            let importance = self.params.ios[i].importance;
            self.set_input_importance(i, importance);
        }

        let Self {
            ref params,
            ref io_sizes,
            ticks,
            histories,
            updates,
            encoders,
            decoders,
            ticks_per_update,
            ..
        } = self;

        // first tick is always 0
        ticks[0] = 0;

        // add input to first layer history
        for i in 0..io_sizes.len() {
            push_front_overwriting(&mut histories[0][i], input_cis[i].clone());
        }

        // set all updates to no update, will be set to true if an update occurred later
        updates.fill(false);

        // forward
        for l in 0..encoders.len() {
            // if is time for layer to tick
            if l == 0 || ticks[l] >= ticks_per_update[l] {
                // reset tick
                ticks[l] = 0;

                // updated
                updates[l] = true;

                let _nullslice = vec![];
                let mut layer_input_cis: Vec<&is> =
                    vec![&_nullslice; encoders[l].visible_layers.len()];

                let mut index = 0;

                for h_li in &histories[l] {
                    for h_lit in h_li {
                        layer_input_cis[index] = h_lit;
                        index += 1;
                    }
                }

                // activate sparse coder
                encoders[l].step(
                    &layer_input_cis,
                    learn_enabled,
                    rng,
                    params.layers[l].encoder,
                );

                // add to next layer's history
                if l < encoders.len() - 1 {
                    let l_next = l + 1;

                    push_front_overwriting(
                        &mut histories[l_next][0],
                        encoders[l].hidden_cis.clone(),
                    );

                    ticks[l_next] += 1;
                }
            }
        }

        let Self {
            actors, i_indices, ..
        } = self;

        // backward
        for l in (0..decoders.len()).rev() {
            if updates[l] {
                let _nullslice = vec![];

                let mut layer_input_cis = vec![&_nullslice; 1 + (l < encoders.len() - 1) as usize];

                layer_input_cis[0] = &encoders[l].hidden_cis;

                let vec_opt: Vec<i32>;

                if l < encoders.len() - 1 {
                    vec_opt = decoders[l + 1][ticks_per_update[l + 1] - 1 - ticks[l + 1]]
                        .hidden_cis
                        .clone(); // sus: clone to satisfy lifetime
                    layer_input_cis[1] = &vec_opt;
                }

                for (d, decoder) in decoders[l].iter_mut().enumerate() {
                    decoder.step(
                        &layer_input_cis,
                        &histories[l][if l == 0 { i_indices[d] } else { 0 }]
                            [if l == 0 { 0 } else { d }],
                        learn_enabled,
                        if l == 0 {
                            params.ios[i_indices[d]].decoder
                        } else {
                            params.layers[l].decoder
                        },
                        rng,
                    );
                }

                if l == 0 {
                    for (d, actor) in actors.iter_mut().enumerate() {
                        actor.step(
                            &layer_input_cis,
                            input_cis[i_indices[d + io_sizes.len()]],
                            reward,
                            learn_enabled,
                            mimic,
                            params.ios[i_indices[d + io_sizes.len()]].actor,
                            rng,
                        );
                    }
                }
            }
        }
    }

    pub fn clear_state(&mut self) {
        self.updates.fill(false);
        self.ticks.fill(0);

        for x in self.histories.iter_mut() {
            for y in x {
                for z in y {
                    z.fill(0);
                }
            }
        }

        for x in self.encoders.iter_mut() {
            x.clear_state();
        }

        for x in self.decoders.iter_mut() {
            for y in x {
                y.clear_state();
            }
        }

        for x in self.actors.iter_mut() {
            x.clear_state();
        }
    }

    /// get the number of layers (encoders)
    pub fn num_layers(&self) -> usize {
        self.encoders.len()
    }

    pub const D_INDEX_INVALID: usize = usize::MAX;

    pub fn io_layer_exists(&self, i: usize) -> bool {
        self.d_indices[i] != Self::D_INDEX_INVALID
    }

    /// retrieve predictions
    pub fn prediction_cis(&self, i: usize) -> &is {
        let Self {
            io_types,
            actors,
            decoders,
            d_indices,
            ..
        } = self;
        if io_types[i] == Action {
            &actors[d_indices[i]].hidden_cis
        } else {
            &decoders[0][d_indices[i]].hidden_cis
        }
    }

    /// retrieve prediction activations
    pub fn prediction_acts(&self, i: usize) -> &fs {
        let Self {
            io_types,
            actors,
            decoders,
            d_indices,
            ..
        } = self;
        if io_types[i] == Action {
            &actors[d_indices[i]].hidden_acts
        } else {
            &decoders[0][d_indices[i]].hidden_acts
        }
    }

    /// importance control
    fn set_input_importance(&mut self, i: usize, importance: f32) {
        let latest_history_len = self.histories[0][i].capacity();
        for t in 0..latest_history_len {
            self.encoders[0].visible_layers[i * latest_history_len + t].importance = importance;
        }
    }
}
