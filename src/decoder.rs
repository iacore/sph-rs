use crate::helpers::*;

pub type VisibleLayerDesc = crate::actor::VisibleLayerDesc;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct VisibleLayer {
    pub weights: Vec<u8>,
    pub input_cis_prev: is,
    pub gates: fs,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Params {
    /// scale of exp
    pub scale: f32,
    /// learning rate
    pub lr: f32,
    /// gate curve
    pub gcurve: f32,
}
impl Default for Params {
    fn default() -> Self {
        Self {
            scale: 64.0,
            lr: 0.05,
            gcurve: 16.0,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Decoder {
    /// size of the output/hidden/prediction
    pub hidden_size: Int3,
    pub hidden_cis: is,
    pub hidden_acts: fs,

    pub hidden_sums: is,
    pub hidden_deltas: fs,

    pub visible_layers: Vec<VisibleLayer>,
    pub visible_layer_descs: Vec<VisibleLayerDesc>,

    /// cartesian product of column coordinates and visible layers. for parallelization.
    pub visible_pos_vlis: Vec<Int3>,
}
impl Decoder {
    pub fn init_random(
        hidden_size: Int3,
        visible_layer_descs: Vec<VisibleLayerDesc>,
        rng: &mut Rng,
    ) -> Self {
        let mut visible_layers = vec![VisibleLayer::default(); visible_layer_descs.len()];

        // pre-compute dimensions
        let num_hidden_columns = hidden_size.x * hidden_size.y;
        let num_hidden_cells = num_hidden_columns * hidden_size.z;

        let mut total_num_visible_columns = 0;

        for (vld, vl) in visible_layer_descs.iter().zip(&mut visible_layers) {
            let num_visible_columns = vld.size.x * vld.size.y;

            total_num_visible_columns += num_visible_columns;

            let diam = vld.radius * 2 + 1;
            let area = diam * diam;

            vl.weights = vec![0; (num_hidden_cells * area * vld.size.z) as usize];

            for x in &mut vl.weights {
                // note[h4]: init_weight_noisei must be odd to make this balanced
                *x = 127u8 + (rng.gen::<u8>() % init_weight_noisei) - init_weight_noisei / 2;
            }

            vl.input_cis_prev = vec![0; num_visible_columns as usize];

            vl.gates = vec![0.; num_visible_columns as usize];
        }

        let hidden_cis = vec![0; num_hidden_columns as usize];

        let hidden_sums = vec![0; num_hidden_cells as usize];
        let hidden_acts = vec![-1.0 /* flag */; num_hidden_cells as usize];

        let hidden_deltas = vec![0.; num_hidden_cells as usize];

        // generate helper buffers for parallelization
        let mut visible_pos_vlis = vec![Int3::default(); total_num_visible_columns as usize];

        let mut index = 0;

        for (vli, vld) in visible_layer_descs
            .iter()
            .enumerate()
        {
            let num_visible_columns = vld.size.x * vld.size.y;

            for i in 0..num_visible_columns {
                visible_pos_vlis[index] = Int3::new(i / vld.size.y, i % vld.size.y, vli.as_());
                index += 1;
            }
        }

        Self {
            hidden_size,
            visible_layer_descs,
            visible_layers,
            hidden_cis,
            hidden_sums,
            hidden_acts,
            hidden_deltas,
            visible_pos_vlis,
        }
    }

    pub fn clear_state(&mut self) {
        self.hidden_cis.fill(0);
        // note[h3] -1. is flag
        self.hidden_acts.fill(-1.);
        for vl in &mut self.visible_layers {
            vl.input_cis_prev.fill(0);
        }
    }

    pub fn step(
        &mut self,
        input_cis: &[&is],
        hidden_target_cis: &is,
        learn_enabled: bool,
        params: Params,
        rng: &mut Rng,
    ) {
        let hidden_size = self.hidden_size;

        let num_hidden_columns = hidden_size.x * hidden_size.y;

        if learn_enabled {
            // update gates
            // optimize: PARALLEL_FOR
            for i in 0..self.visible_pos_vlis.len() {
                let pos = self.visible_pos_vlis[i].xy();
                let vli = self.visible_pos_vlis[i].z;

                self.update_gates(pos, vli.as_(), params);
            }

            let base_seed: u64 = rng.gen();

            // optimize: PARALLEL_FOR
            for i in 0..num_hidden_columns {
                let mut rng = Rng::seed_from_u64(base_seed.wrapping_add(i as u64));

                self.learn(
                    Int2::new(i / hidden_size.y, i % hidden_size.y),
                    hidden_target_cis,
                    &mut rng,
                    params,
                );
            }
        }

        // optimize: PARALLEL_FOR
        for i in 0..num_hidden_columns {
            self.forward(
                Int2::new(i / hidden_size.y, i % hidden_size.y),
                input_cis,
                params,
            );
        }

        // copy to prevs
        for (vl, input_cis_vl) in self.visible_layers.iter_mut().zip(input_cis) {
            vl.input_cis_prev = (*input_cis_vl).clone();
        }
    }

    fn forward(&mut self, column_pos: Int2, input_cis: &[&is], params: Params) {
        let Self {
            ref hidden_size,
            ref visible_layers,
            ref visible_layer_descs,
            hidden_sums,
            hidden_acts,
            hidden_cis,
            ..
        } = self;

        let hidden_column_index = address2(column_pos, Int2::new(hidden_size.x, hidden_size.y));

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_sums[hidden_cell_index as usize] = 0;
        }

        let mut count = 0;

        for ((vld, vl), vl_input_cis) in visible_layer_descs
            .iter()
            .zip(visible_layers)
            .zip(input_cis)
        {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            count += (iter_upper_bound.x - iter_lower_bound.x + 1)
                * (iter_upper_bound.y - iter_lower_bound.y + 1);

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci = vl_input_cis[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_value = offset.y
                        + diam * (offset.x + diam * (in_ci + vld.size.z * hidden_column_index));
                    let wi_start = hidden_size.z * wi_value;

                    for hc in 0..hidden_size.z {
                        let hidden_cell_index = hc + hidden_cells_start;
                        let wi = hc + wi_start;
                        // optimize: SIMD
                        hidden_sums[hidden_cell_index as usize] += vl.weights[wi as usize] as i32;
                    }
                }
            }
        }

        let count = count as f32;

        let mut max_index = 0;
        let mut max_activation = 0.0f32;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            let activation = hidden_sums[hidden_cell_index as usize] as f32 / (count * 255.);

            hidden_acts[hidden_cell_index as usize] = activation;

            if activation > max_activation {
                max_activation = activation;
                max_index = hc;
            }
        }

        let mut total = 0.0f32;

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] =
                ((hidden_acts[hidden_cell_index as usize] - max_activation) * params.scale).exp();

            total += hidden_acts[hidden_cell_index as usize];
        }

        let total_inv = 1.0 / f32::max(limit_small, total);

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_acts[hidden_cell_index as usize] *= total_inv;
        }

        hidden_cis[hidden_column_index as usize] = max_index.as_();
    }

    fn update_gates(&mut self, column_pos: Int2, vli: usize, params: Params) {
        let Self {
            visible_layers,
            ref visible_layer_descs,
            ref hidden_size,
            ..
        } = self;

        let vl = &mut visible_layers[vli];
        let vld = visible_layer_descs[vli];

        let diam = vld.radius * 2 + 1;

        let visible_column_index = address2(column_pos, Int2::new(vld.size.x, vld.size.y));

        // projection
        let v_to_h = Float2::new(
            (hidden_size.x as f32) / (vld.size.x as f32),
            (hidden_size.y as f32) / (vld.size.y as f32),
        );

        let h_to_v = Float2::new(
            (vld.size.x as f32) / (hidden_size.x as f32),
            (vld.size.y as f32) / (hidden_size.y as f32),
        );

        let reverse_radii = Int2::new(
            (v_to_h.x * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
            (v_to_h.y * (vld.radius * 2 + 1) as f32 * 0.5).ceil().as_(),
        );

        let hidden_center = project(column_pos, v_to_h);

        // lower corner
        let field_lower_bound = Int2::new(
            hidden_center.x - reverse_radii.x,
            hidden_center.y - reverse_radii.y,
        );

        // bounds of receptive field, clamped to input size
        let iter_lower_bound = Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
        let iter_upper_bound = Int2::new(
            min(hidden_size.x - 1, hidden_center.x + reverse_radii.x),
            min(hidden_size.y - 1, hidden_center.y + reverse_radii.y),
        );

        let in_ci_prev = vl.input_cis_prev[visible_column_index as usize];

        let half_byte_inv = 1.0f32 / 127.0;

        let mut sum = 0.0f32;
        let mut count = 0;

        for ix in iter_lower_bound.x..=iter_upper_bound.x {
            for iy in iter_lower_bound.y..=iter_upper_bound.y {
                let hidden_pos = Int2::new(ix, iy);
                let hidden_column_index =
                    address2(hidden_pos, Int2::new(hidden_size.x, hidden_size.y));

                let visible_center = project(hidden_pos, h_to_v);

                if in_bounds(
                    column_pos,
                    visible_center - Int2::splat(vld.radius),
                    visible_center + Int2::splat(vld.radius + 1),
                ) {
                    let offset = column_pos - visible_center + Int2::splat(vld.radius);

                    let wi_start = hidden_size.z
                        * (offset.y
                            + diam
                                * (offset.x
                                    + diam * (in_ci_prev + vld.size.z * hidden_column_index)));

                    for hc in 0..hidden_size.z {
                        let wi = hc + wi_start;
                        // optimize: SIMD
                        let w = (127.0 - vl.weights[wi as usize] as f32) * half_byte_inv;
                        sum += w.abs();
                    }

                    count += hidden_size.z;
                }
            }
        }

        let sum = sum / max(1, count) as f32;

        vl.gates[visible_column_index as usize] = (-sum * params.gcurve).exp();
    }

    fn learn(&mut self, column_pos: Int2, hidden_target_cis: &is, rng: &mut Rng, params: Params) {
        let Self {
            ref hidden_size,
            ref hidden_acts,
            hidden_deltas,
            visible_layers,
            ref visible_layer_descs,
            ..
        } = self;
        let hidden_column_index = address2(column_pos, Int2::new(hidden_size.x, hidden_size.y));

        let hidden_cells_start = hidden_column_index * hidden_size.z;

        // check if has acts computed (ran at least once) by checking for flag value
        // note[h3] -1. is flag
        if hidden_acts[hidden_cells_start as usize] == -1. {
            return;
        }

        let target_ci = hidden_target_cis[hidden_column_index as usize];

        for hc in 0..hidden_size.z {
            let hidden_cell_index = hc + hidden_cells_start;

            hidden_deltas[hidden_cell_index as usize] = params.lr
                * 255.0
                * ((hc == target_ci) as u8 as f32 - hidden_acts[hidden_cell_index as usize]);
        }

        for (vld, vl) in visible_layer_descs.iter().zip(visible_layers) {
            let diam = vld.radius * 2 + 1;

            // projection
            let h_to_v = Float2::new(
                (vld.size.x as f32) / (hidden_size.x as f32),
                (vld.size.y as f32) / (hidden_size.y as f32),
            );

            let visible_center = project(column_pos, h_to_v);

            // lower corner
            let field_lower_bound =
                Int2::new(visible_center.x - vld.radius, visible_center.y - vld.radius);

            // bounds of receptive field, clamped to input size
            let iter_lower_bound =
                Int2::new(max(0, field_lower_bound.x), max(0, field_lower_bound.y));
            let iter_upper_bound = Int2::new(
                min(vld.size.x - 1, visible_center.x + vld.radius),
                min(vld.size.y - 1, visible_center.y + vld.radius),
            );

            for ix in iter_lower_bound.x..=iter_upper_bound.x {
                for iy in iter_lower_bound.y..=iter_upper_bound.y {
                    let visible_column_index =
                        address2(Int2::new(ix, iy), Int2::new(vld.size.x, vld.size.y));

                    let in_ci_prev = vl.input_cis_prev[visible_column_index as usize];

                    let offset = Int2::new(ix - field_lower_bound.x, iy - field_lower_bound.y);

                    let wi_value = offset.y
                        + diam
                            * (offset.x + diam * (in_ci_prev + vld.size.z * hidden_column_index));
                    let wi_start = hidden_size.z * wi_value;

                    let gate = vl.gates[visible_column_index as usize];

                    for hc in 0..hidden_size.z {
                        let hidden_cell_index = hc + hidden_cells_start;
                        let wi = hc + wi_start;
                        // optimize: SIMD
                        vl.weights[wi as usize] = rand_roundf(
                            rng,
                            vl.weights[wi as usize] as f32
                                + hidden_deltas[hidden_cell_index as usize] * gate,
                        )
                        .clamp(0., 255.) as u8;
                    }
                }
            }
        }
    }
}
